#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse
import fix_moodle_xml

# LuaLaTeX command
texargs = "--shell-escape --synctex=-1 --interaction=nonstopmode --enable-write18 "
lualatexcmd = "lualatex " + texargs

# pdfLaTeX command
pdflatexcmd = "pdflatex " + texargs

def compile_chapter(chap):
  
  if chap == "rwthquiz":
    # standalone does not like tikzexternalize. Therefore, if main document is compiled, we need to compile first with pdflatex
    # which compiles the tikz pictures and stores them in the tmp folder
    print("Invoking pdflatex")
    os.system(pdflatexcmd  + chap + ".tex")
    print("Done!")

  print("Invoking lualatex")
  os.system(lualatexcmd  + chap + ".tex")
  print("Done!")

  print("Invoking fix_moodle_xml.py")
  fix_moodle_xml.do_substitution(chap + "-moodle.xml", chap + "-moodle.xml")
  print("Done!")


if __name__ == '__main__':
  
  parser = argparse.ArgumentParser(description='''
  compiles either the full quiz (rwthquiz.tex) or a certain chapter (e.g. chapter1.tex). 
  Since the package `standalone` and `tikzexternalize` are not working with `lualatex` (needed to create the XML file), 
  pdflatex is called first to create the tikz figures and then lualatex is called afterwards to create the XML file.
  ''')
  parser.add_argument("-c", "--chap", type=str, default="rwthquiz",
                      help="which chapter to compile. Default: rwthquiz, meaning all chapters are compiled.")
  args = parser.parse_args()
  
  compile_chapter(args.chap)
