# LaTeX2MoodleQuiz

Welcome! This repository basically forks and extends CTANs `moodle.sty` (<https://ctan.org/pkg/moodle>) allowing to specify Moodle Quiz questions via LaTeX.
After compiling, a PDF and a XML file ready to upload to Moodle are created.  
Please make sure to read the [documentation](http://mirrors.ctan.org/macros/latex/contrib/moodle/moodle.pdf) of `moodle.sty` first!

Exemplary outputs can be downloaded here:
* [rwthquiz.pdf](https://git.rwth-aachen.de/IENT/latex2moodlequiz/-/jobs/artifacts/master/raw/rwthquiz.pdf?job=build)
* [rwthquiz-moodle.xml](https://git.rwth-aachen.de/IENT/latex2moodlequiz/-/jobs/artifacts/master/raw/rwthquiz-moodle.xml?job=build)

## Installation

Besides a tex-distribution (LuaLaTeX and pdfLaTeX are needed specifically), Ghostscript and OpenSSL are needed to embed graphics into the XML file. Python is needed to fix wrong characters in the XML file (e.g. Umlaute).

1. Windows:
    * Install `texlive2018`
    * Install Ghostscript (try to install [`GS 9.26`](https://github.com/ArtifexSoftware/ghostpdl-downloads/releases/tag/gs926): Download and install `gs926aw64.exe` and add `C:\Program Files\gs\gs9.26\bin` to User Path Variable
    * Install [OpenSSL](https://slproweb.com/products/Win32OpenSSL.html) and add `C:\Program Files\OpenSSL-Win64\bin` to User Path Variable
    * Install Python (needed e.g. for `fix_moodle_xml.py`, `compile.py`)
2. Linux: T.B.D.

You can then run `compile.py` (see below) to compile the quiz!

## Components

* `rwth-moodle.sty` is a fork from CTANs `moodle.sty` (Copyright 2016 Anders O. F. Hendrickson) with some small fixes and extensions (e.g. correct order of figures in multiple choice answers)
* `compile.py` will automatically compile the main `.tex`-file with pdfLaTeX to compile TikZ figures and LuaLaTeX to compile the XML file. `fix_moodle_xml.py` (see below) is called in a subsequent step to fix the XML.  
It is also possible to compile specific chapters (e.g. `chapterX`) with

    ```bash
    python compile.py --chap chapterX
    ```

    Note that pdfLaTeX is not invoked in this case

* `fix_moodle_xml.py`: The original `moodle.sty` and its extension `rwth-moodle.sty` do not support equations in item specific feedbacks. The script `fix_moodle_xml.py` searches for wrong characters and substitutes them by the correct one.  
It is possible to specify input and output XML files via

    ```bash
    python fix_moodle_xml.py -i input.xml -o output.xml
    ```

## Images

To have images in the quiz questions, TikZ is used. The tex command creates a PDF (`tikzexternalize` is enabled), which is converted to a PNG file via Ghostscript. Then, the PNG is base64-encoded by OpenSSL to be included in the XML file.

## Original Authors

Christian Rohlfing, Julian Becker, Ingrid Reißel, IENT RWTH Aachen  
Mathias Wien, LfB RWTH Aachen

## Who do I talk to?

[Christian Rohlfing](mailto:rohlfing@ient.rwth-aachen.de)